
let bankBalance = 500;
let pay =  0;
let outstandingLoan =  0;
let balanceNow  =   document.getElementById("balance")
let outstandingBalance  =   document.getElementById("outstanding")
let payBalance  =   document.getElementById("pay")
let removeLoan  =   document.getElementById("remove_loan")
let elementPicked;

let nameLaptop  =   document.getElementById("name_laptop")
let price  =   document.getElementById("price")
let image  =   document.getElementById("picture")
let desc  =   document.getElementById("description")
let feature  =   document.getElementById("feature")

class Laptop {
    constructor(name, price,desc,feature,image) {
      this.name = name
      this.price = price
      this.desc = desc
      this.feature = feature
      this.image=image
    }
  }

let listWithLaptops =[new Laptop("macBook", 3500, "Apple MacBook Pro is a macOS laptop with a 13.30-inch display that has a resolution of 2560x1600 pixels. It is powered by a Core i5 processor and it comes with 12GB of RAM.","Continuity Camera. Dark Mode. Desktop Stacks. Dynamic Desktops.", "macbook.jpg"),
new Laptop("acerSwift", 7800, "Slim, lightweight and stylish, the Swift 3 is the ideal laptop for working on the move. The latest NVIDIA® GPU, Intel® CPU and long battery life are housed inside a choice of displays","Slim and light.Dark Mode.Amazing UserDesign.Dynamic Desktops.","acer.jpg"),
new Laptop("hp", 4500, "The hardware links in the left side of the HP System Information window will provide information about the processor type and speed, the manufacturer and model of the system board,", "Slim and light. Dark Mode. Amazing UserDesign. Dynamic Desktops.","hp.jpg"),
new Laptop("lenovo", 1100, "Lenovo ThinkPad is a Windows 10 laptop with a 14.00-inch display that has a resolution of 1920x1080 pixels. It is powered by a Core i7 processor and it comes with 12GB of RAM.", "Display. Size 14.00-inch. ResolutionProcessor: Intel Core i7 6600U. Base Clock Speed. Memory RAM 12GB","lenovo.jpg")]


let alreadyHadLoan  =   false;
let payedBack = true;
let selector = document.getElementById("laptop_selector")

function takeLoan(){
    let loanWish = prompt("How much do you want to loan: ", "");
    if ( loanWish  != null) {
        if ( loanWish  != "") {
            if(alreadyHadLoan == true && payedBack == false ){
               
                alert( "Buy a computer and pay us back before you can take another loan")
            }
            else{
                if(loanWish> (bankBalance*2) ){
                
                    alert( "You can only take loans less than:")
                }
                else{
                    bankBalance = +bankBalance + +loanWish;
                    outstandingLoan=loanWish;
                    balanceNow.innerText=bankBalance + "  kr"
                    showOutstandingBalance()
                    removeLoan.style.display='block'
                    alreadyHadLoan= true;
                    payedBack=false;
                }
            }
        }
    }
    
}

function showLoan(){
    balanceNow.innerText=bankBalance+ "  kr"
}
function showPay(){
    payBalance.innerText=pay+ "  kr"
}
function showOutstandingBalance(){
    outstandingBalance.innerText= " Outstanding Balance : " + outstandingLoan + "  kr"
}
function increasePay(){
    pay= +pay + 100
    payBalance.innerText= pay+ "  kr"
}


function transferPay(){
    if(alreadyHadLoan==true){
        let prosentage= (pay*10)/100
        checkOutstandingLoan(prosentage)
        showLoan()
        showPay()
        showOutstandingBalance()
   }else{
        bankBalance = +bankBalance + +pay
        pay=0;
        showLoan()
        showPay()
   }
}

function checkOutstandingLoan(prosentage){
    if(prosentage >= outstandingLoan){
        outstandingLoan = 0
        alreadyHadLoan=false
        payedBack=true
        
    }
    else{
        outstandingLoan = +outstandingLoan - prosentage    
        
    }
    pay=0 
    bankBalance= +bankBalance + +(pay-prosentage)
    
}

function removeEntireLoan(){
    if(pay >= outstandingLoan){
        outstandingLoan = 0
        alreadyHadLoan=false
        payedBack=true
    }
    else{
        outstandingLoan = +outstandingLoan - pay
       
    }
    pay=0;
    showLoan()
    showPay()
    showOutstandingBalance()

}


function showElement(selectObject){
    let selected_option = selectObject.value; 
    listWithLaptops.forEach(element => {
        
        if(element.name == selected_option){
            elementPicked=element
            nameLaptop.innerText=element.name;
            price.innerText= element.price + "  kr"
            desc.innerText= element.desc
            feature.innerText="Features : \n" +element.feature
            image.style.display='block'
            image.src= element.image
        }
    });
}

function buyLaptop(){
    if(bankBalance < elementPicked.price){
        alert(" You don't have enough money in you bank balance")
    }
    else{
        bankBalance = +bankBalance - elementPicked.price
        alert(" now you have a Laptop, congrats! ")
        showLoan()

    }
}
